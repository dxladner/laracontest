@extends('layouts.admin')

@section('title')
Winners
@stop

@section('head')

@stop

@section('content')
	<section class="wrapper">

		<div class="row">
			<div class="col-sm-12">

				<div class="panel">
					<div class="panel-heading">Search <!-- <a href="/admin/email-subscriptions/create" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-plus-sign"></i> Add New</a> -->
						<a href="{{ url('/registrations') }}" class="btn btn-success btn-xs pull-right">
              <i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
					</div>

					@if( Session::has('reports') )
						@if($errors->any())
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<ul>
									{{ implode('', $errors->all('<li class="error">:message</li>')) }}
								</ul>
							</div>
						@endif
					@endif


					<div class="panel-body">
						<table class="table table-striped table-hover">
							<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>State</th>
								<th>Date</th>
								<th>Actions</th>
							</tr>
							</thead>

							@if(!empty($registrations[0]))
								@foreach($registrations as $registration)
									<tr>
										<td><a href="/admin/registrations/{{ $registration->id }}/edit">{{ $registration->fullName() }}</a></td>
										<td>
											<?php $emaillink = preg_split("/\@/", $registration->email); ?>
											{{$emaillink[0]}} <i class="fa fa-plus" aria-hidden="true"></i> {{$emaillink[1]}}
										</td>
										<td>{{ $registration->state }}</td>
										<td>{{ $registration->created_at->format('F j, Y g:i A') }}</td>
										<td>
											<a href="{{ url('/registrations/'. $registration->id) }}/edit" class="btn btn-info btn-xs">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                      </a>
										</td>

									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="7">
										<div class="alert alert-info">No winners were found.</div>
									</td>
								</tr>

							@endif
						</table>
						<div class="pull-right">
							@if(!empty($winners[0]))
								{{ $winners->appends(Request::except('page', '_token'))->links() }}
							@endif
						</div>

					</div> <!-- /.panel-body -->
				</div> <!-- /.panel -->
			</div> <!-- /.col-sm-12 -->
		</div> <!-- /.row -->
	 </section> <!-- /.container -->
@stop

@section('script')

@stop
