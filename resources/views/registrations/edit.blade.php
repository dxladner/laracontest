@extends('layouts.admin')

@section('title')
Edit Registration
@stop

@section('head')
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="{{ url('/') }}/css/datetimepicker.css">

@stop

@section('content')
	<section class="wrapper">

		<div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-heading">
						Edit Registration
						<a href="{{ url('/registrations') }}" class="btn btn-success btn-xs pull-right">
              <i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
						<button style="margin-right: 5px;" role="button" type="button" class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#deleteModal">
              <i class="fa fa-trash" aria-hidden="true"></i>
              Delete
            </button>
					</div>

					<div class="panel-body">
            <form action="{{ url('/registrations/'. $registration->id) }}" method="POST">
                <input type="hidden" name="_method" value="patch">
                {{ csrf_field() }}
  							@if($errors->any())
  								<div class="alert alert-danger">
  									<ul>
  										{{ implode('', $errors->all('<li class="error">:message</li>')) }}
  									</ul>
  								</div>
  							@endif

											<!-- Col 1 -->
			                <div class="col-xs-6">
		                    <div class="form-group text-left <?php if($errors->has('first_name')) { echo 'has-error'; } ?>">
                          <label for="first_name">FIRST NAME</label>
		                      <input type="text" class="form-control input-text" id="first_name" name="first_name" value="{{ $registration->first_name }}" />
		                    </div>
		                    <div class="form-group text-left <?php if($errors->has('last_name')) { echo 'has-error'; } ?>">
                          <label for="last_name">LAST NAME</label>
                          <input type="text" class="form-control input-text" id="last_name" name="last_name" value="{{ $registration->last_name }}" />
		                    </div>
		                    <div class="form-group text-left <?php if($errors->has('phone')) { echo 'has-error'; } ?>">
                          <label for="phone">PHONE</label>
                          <input type="text" class="form-control input-text" id="phone" name="phone" value="{{ $registration->phone }}" />
		                    </div>
		                    <div class="form-group text-left <?php if($errors->has('email')) { echo 'has-error'; } ?>">
                          <label for="email">EMAIL</label>
                          <input type="text" class="form-control input-text" id="email" name="email" value="{{ $registration->email }}" />
									      </div>
                        <div class="form-group text-left <?php if($errors->has('proclub_number')) { echo 'has-error'; } ?>">
                          <label for="proclub_number">Proclub #</label>
                          <input type="text" class="form-control" id="proclub_number" name="proclub_number" value="{{ $registration->proclub_number }}">
                        </div>
                        <div class="form-group text-left <?php if($errors->has('account_number')) { echo 'has-error'; } ?>">
                          <span style="font-size: 18px;color: red;">OR</span>
                          <label for="account_number">Account #</label>
                          <input type="text" class="form-control" id="account_number" name="account_number" value="{{ $registration->account_number }}">
                        </div>
					            </div><!-- End Col 1 -->

					            <!-- Col 2 -->
			                <div class="col-xs-6">
			                	<div class="form-group text-left <?php if($errors->has('address_one')) { echo 'has-error'; } ?>">
                          <label for="address_one">ADDRESS ONE</label>
                          <input type="text" class="form-control input-text" id="address_one" name="address_one" value="{{ $registration->address_one }}" />
					              </div>
                        <div class="form-group text-left <?php if($errors->has('address_two')) { echo 'has-error'; } ?>">
                          <label for="address_two">ADDRESS TWO</label>
                          <input type="text" class="form-control input-text" id="address_two" name="address_two" value="{{ $registration->address_two }}" />
					              </div>
			                    <div class="row">
			                    	<div class="form-group text-left col-xs-4 <?php if($errors->has('city')) { echo 'has-error'; } ?>">
                              <label for="city">CITY</label>
                              <input type="text" class="form-control input-text" id="city" name="city" value="{{ $registration->city }}" />
				                    </div>
				                    <div class="form-group col-xs-4 text-left <?php if($errors->has('state')) { echo 'has-error'; } ?>">
                              <label for="state">State</label>
                              <select id="state" name="state" class="form-control">
                                <option value="{{ $registration->state }}">{{ $registration->state }}</option>
                     					  <option value="TX">Texas</option>
                                <option value="AL">Alabama</option>
                                <option value="LA">Louisiana</option>
                              </select>
				                    </div>
				                     <div class="form-group col-xs-4 text-left <?php if($errors->has('zip')) { echo 'has-error'; } ?>">
                                 <label for="zip">ZIP</label>
                                 <input type="text" class="form-control input-text" id="zip" name="zip" value="{{ $registration->zip }}" />
				                    </div>

				                </div><!--End Row-->
                        <div class="row">
                          <div class="form-group col-xs-4 text-left <?php if($errors->has('profession')) { echo 'has-error'; } ?>">
                            <label for="profession">Profession</label>
                            <select id="profession" name="profession" class="form-control">
                              <option value="{{ $registration->profession }}">{{ $registration->profession }}</option>
                              <option value="Salon Owner">Salon Owner</option>
                              <option value="Cosmetologist">Cosmetologist</option>
                              <option value="Esthetician">Esthetician</option>
                              <option value="Nail Technician">Nail Technician</option>
                            </select>
                          </div>
                            <div class="form-group col-xs-4 text-left <?php if($errors->has('dob')) { echo 'has-error'; } ?>">
                            <label for="dob">DOB</label>
                            <input type="text" class="form-control" id="dob" name="dob" value="{{ $registration->dob }}" />
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group checkbox col-xs-12 text-left">
                            <label>
                              <input checked="checked" name="verify_receive_emails" type="checkbox" value="1" />
                              I would like to receive emails about special promotions, education courses, tips and trends.
                            </label>
                          </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-primary btn-sm">Save Registration</button>
                        </div>
			              </div><!-- End Col 2-->

						  </form>
					  </div>
					</div>
				</div> <!-- /.panel -->
			</div>
		</div>

	 </section> <!-- /.container -->


<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete Registration</h4>
      </div>
      <div class="modal-body">
      	<p>Are you sure you want to delete this registration?</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">Cancel</button>
        <form role="form" method="POST" action="{{ url('/registrations/delete/'. $registration->id) }}" style="display: inline;">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
            <button style="margin-right: 5px;" type="submit" data-toggle="tooltip" data-placement="left" title="Delete" class="btn-success btn">
              <i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>

@stop

@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="{{ url('/') }}/js/datetimepicker.js"></script>
<script>
    $( ".datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "1914:2015",
      dateFormat: "yy-mm-dd"
    });
</script>

@stop
