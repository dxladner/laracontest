@extends('layouts.admin')

@section('title')
Email Registrations
@stop

@section('head')
<link href="{{ url('/') }}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
@stop

@section('content')
	<section class="wrapper">

		<div class="row">
			<div class="col-sm-12">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
							<ul>
									@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
									@endforeach
							</ul>
					</div>

				@elseif( Session::get('error') )

						<div class="alert alert-danger">
								<ul><li>{{ Session::get('error') }}</li></ul>
						</div>

				 @elseif( Session::get('register_error') )
						<div class="alert alert-danger">
								<ul><li>{{ Session::get('register_error') }}</li></ul>
						</div>
				@endif
				<div class="panel">
					<div class="panel-heading">Registrations
            <form class="form-inline pull-right form-search" action="{{ url('/registrations/search') }}" method="post">
							{{ csrf_field() }}
              <div class="input-group">
                <input name="q" type="text" class="form-control input-sm" placeholder="Regstration Search" />
								<span class="input-group-btn">
                  <button class="btn btn-primary btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
								</span>
							</div><!-- /input-group -->
            </form>
            <a href="{{ url('/registrations/create') }}" class="btn btn-success">Add Registration</a>
					</div>



					<nav class="tab-dark">
            <form class="form-inline col-xs-9 form-report" action="{{ url('/registrations') }}" method="get">
						<div class="row">
							<?php
								$date_start = Session::get('date_start');
								$date_end = Session::get('date_end');
							?>
							<div class="col-xs-4">
								<div class="input-group input-group-sm">
                  <input type="text" class="form-control input-sm datepicker" id="date_start" name="date_start" placeholder="Start Date" />
									<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group input-group-sm">
                  <input type="text" class="form-control input-sm datepicker" id="date_end" name="date_end" placeholder="End Date" />
									<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
								</div>

							</div>
							<div class="col-xs-4">
								<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-repeat" aria-hidden="true"></i> Run</button>
							</div>
						</div>
          </form>
					
						<?php $disabled = 'disabled' ?>
						@if($date_start != '' && $date_end != '')
							<?php $disabled = ''; ?>
						@endif

            <form class="form-inline col-xs-3 form-report" action="{{ url('/registrations/reports') }}" method="get">
							{{ csrf_field() }}
					    <input type="hidden" value="{{ $date_start }}" id="date_start" name="date_start"  />
              <input type="hidden" value="{{ $date_end }}" id="date_end" name="date_end"  />
							<span style="color: white;">Select date range to the left first </span>
              <button type="submit" class="btn btn-default btn-sm {{$disabled}}">
                <i class="fa fa-download" aria-hidden="true"></i>
                Download
              </button>
						</form>
					</nav>

					@if( Session::has('success') )
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							{{ Session::get('success') }}
						</div>
					@endif

					<div class="panel-body">
						<table class="table table-striped table-hover">
							<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>Date</th>
								<th>Actions</th>
							</tr>
							</thead>
							<?php
								//$winner = Config::get('constants.WINNER');
							?>
							@if(!empty($registrations[0]))
								@foreach($registrations as $registration)
									<tr>
										<td><a href="{{ url('/registrations/'. $registration->id) }}/edit">{{ $registration->fullName() }}</a></td>
										<td>
											<?php $emaillink = preg_split("/\@/", $registration->email); ?>
											{{$emaillink[0]}} <i class="fa fa-plus" aria-hidden="true"></i> {{$emaillink[1]}}
										</td>
										<td>{{ $registration->created_at->format('F j, Y g:i A') }}</td>
										<td>

											<a href="{{ url('/registrations/'. $registration->id) }}/edit" data-toggle="tooltip" data-placement="left" title="Edit" class="btn btn-info btn-xs">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                      </a>
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="5">
										<div class="alert alert-info">No Registrations were found.</div>
									</td>
								</tr>

							@endif
						</table>
						<div class="pull-right">
							@if( $registrations->count() )
								{{ $registrations->appends(Request::except('page', '_token'))->links() }}
							@endif
						</div>

					</div> <!-- /.panel-body -->
				</div> <!-- /.panel -->
			</div> <!-- /.col-sm-12 -->
		</div> <!-- /.row -->
	 </section> <!-- /.container -->
@stop

@section('script')
<script src="{{url('/') }}/js/bootstrap-datetimepicker.min.js"></script>
<script>

	$(".datepicker").datetimepicker({
        format: "yyyy-mm-dd",
        //showMeridian: true,
        autoclose: true,
        //todayBtn: true,
        startView: 2,
        minView: 2,
        maxView: 2,
    });

</script>
@stop
