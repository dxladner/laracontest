@extends('layouts.admin')

@section('title')
Create Registration
@stop

@section('head')
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="{{ url('/') }}/css/datetimepicker.css">
@stop

@section('content')
	<section class="wrapper">

		<div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-heading">Add new registration <a href="{{ url('/registrations') }}" class="btn btn-success btn-xs pull-right">
            <i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a></div>
					    <div class="panel-body">

								@if (count($errors) > 0)
									<div class="alert alert-danger">
											<ul>
													@foreach ($errors->all() as $error)
															<li>{{ $error }}</li>
													@endforeach
											</ul>
									</div>

				            @elseif( Session::get('error') )

				                <div class="alert alert-danger">
				                    <ul><li>{{ Session::get('error') }}</li></ul>
				                </div>

				             @elseif( Session::get('register_error') )
				                <div class="alert alert-danger">
				                    <ul><li>{{ Session::get('register_error') }}</li></ul>
				                </div>
				            @endif

                      <form id="registration" name="registration" action="{{ url('/registrations/create') }}" method="POST">
				            	<div class="row">
												{{ csrf_field() }}
				            		<!-- Col 1 -->
					                <div class="col-xs-6">
					                    <div class="form-group text-left <?php if($errors->has('first_name')) { echo 'has-error'; } ?>">
                                <label for="first_name">FIRST NAME</label>
					                      <input type="text" id="first_name" name="first_name" class="form-control input-text"  />
					                    </div>
					                    <div class="form-group text-left <?php if($errors->has('last_name')) { echo 'has-error'; } ?>">
                                <label for="last_name">LAST NAME</label>
                                <input type="text" id="last_name" name="last_name" class="form-control input-text"  />
					                    </div>

															<div class="form-group text-left <?php if($errors->has('proclub_number')) { echo 'has-error'; } ?>">
					                      <label for="proclub_number">Proclub #</label>
					                      <input type="text" class="form-control" id="proclub_number" name="proclub_number" />
															</div>
															<span style="font-size: 18px;color: red;">OR</span>
															<div class="form-group text-left <?php if($errors->has('account_number')) { echo 'has-error'; } ?>">
					                      <label for="account_number">Account #</label>
					                      <input type="text" class="form-control" id="account_number" name="account_number" />
					                    </div>
					                    <div class="form-group text-left <?php if($errors->has('email')) { echo 'has-error'; } ?>">
                                <label for="email">EMAIL</label>
                                <input type="email" id="email" name="email" class="form-control input-text"  />
									            </div>
					                </div><!-- End Col 1 -->

													<div class="col-xs-6">
					                	<div class="form-group text-left <?php if($errors->has('address_one')) { echo 'has-error'; } ?>">
		                          <label for="address_one">ADDRESS ONE</label>
		                          <input type="text" class="form-control input-text" id="address_one" name="address_one" />
							              </div>
		                        <div class="form-group text-left <?php if($errors->has('address_two')) { echo 'has-error'; } ?>">
		                          <label for="address_two">ADDRESS TWO</label>
		                          <input type="text" class="form-control input-text" id="address_two" name="address_two" />
							              </div>
					                    <div class="row">
					                    	<div class="form-group text-left col-xs-4 <?php if($errors->has('city')) { echo 'has-error'; } ?>">
		                              <label for="city">CITY</label>
		                              <input type="text" class="form-control input-text" id="city" name="city" />
						                    </div>
						                    <div class="form-group col-xs-4 text-left <?php if($errors->has('state')) { echo 'has-error'; } ?>">
		                              <label for="state">State</label>
		                              <select id="state" name="state" class="form-control">
		                     					  <option value="TX">Texas</option>
		                                <option value="AL">Alabama</option>
		                                <option value="LA">Louisiana</option>
		                              </select>
						                    </div>
						                     <div class="form-group col-xs-4 text-left <?php if($errors->has('zip')) { echo 'has-error'; } ?>">
		                                 <label for="zip">ZIP</label>
		                                 <input type="text" class="form-control input-text" id="zip" name="zip" />
						                    </div>

						                </div><!--End Row-->
		                        <div class="row">
		                          <div class="form-group col-xs-4 text-left <?php if($errors->has('profession')) { echo 'has-error'; } ?>">
		                            <label for="profession">Profession</label>
		                            <select id="profession" name="profession" class="form-control">
		                              <option value="Salon Owner">Salon Owner</option>
		                              <option value="Cosmetologist">Cosmetologist</option>
		                              <option value="Esthetician">Esthetician</option>
		                              <option value="Nail Technician">Nail Technician</option>
		                            </select>
		                          </div>
		                            <div class="form-group col-xs-4 text-left <?php if($errors->has('dob')) { echo 'has-error'; } ?>">
		                            <label for="dob">DOB</label>
		                            <input type="text" class="form-control" id="dob" name="dob" />
		                          </div>
		                        </div>
		                        <div class="row">
		                          <div class="form-group checkbox col-xs-12 text-left">
		                            <label>
		                              <input checked="checked" name="verify_receive_emails" type="checkbox" value="1" />
		                              I would like to receive emails about special promotions, education courses, tips and trends.
		                            </label>
		                          </div>
		                        </div>
														<div class="form-group text-right">
                                <button type="submit" class="btn btn-primary btn-sm">Add Registration</button>
							              </div>
					              </div><!-- End Col 2-->
				            	</div><!--End Row-->

				        </form>

					  </div>
					</div>
				</div> <!-- /.panel -->
			</div>
		</div>

	 </section> <!-- /.container -->
@stop

@section('script')

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="{{ url('/') }}/js/datetimepicker.js"></script>
<script>
    $( ".datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "1914:2015",
      dateFormat: "yy-mm-dd"
    });
</script>
@stop
