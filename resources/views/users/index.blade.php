@extends('layouts.admin')

@section('content')

<div class="container usercontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>

                <div class="panel-body">
                  <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Date</th>
                      <th>Actions</th>
                    </tr>
                    </thead>
                    <?php
                      //$winner = Config::get('constants.WINNER');
                    ?>
                    @if(!empty($users[0]))
                      @foreach($users as $user)
                        <tr>
                          <td>{{ $user->name }}</td>
                          <td>
                            <?php $emaillink = preg_split("/\@/", $user->email); ?>
                            {{$emaillink[0]}} <i class="fa fa-plus" aria-hidden="true"></i> {{$emaillink[1]}}
                          </td>
                          <td>{{ $user->created_at->format('F j, Y g:i A') }}</td>
                          <td>

                            <a href="{{ url('/users/'. $user->id) }}/edit" data-toggle="tooltip" data-placement="left" title="Edit" class="btn btn-info btn-xs">
                              <i class="fa fa-pencil" aria-hidden="true"></i>
                            </a>
                          </td>
                        </tr>
                      @endforeach
                    @else
                      <tr>
                        <td colspan="5">
                          <div class="alert alert-info">No Users were found.</div>
                        </td>
                      </tr>

                    @endif
                  </table>
                </div>
            </div>
            <a href="{{ url('/users/create') }}" class="btn btn-success">Add User</a>
        </div>
    </div>
</div>

@endsection
