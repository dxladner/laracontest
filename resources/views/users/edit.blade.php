@extends('layouts.admin')

@section('title')
Edit User
@stop

@section('content')
	<section class="wrapper">

		<div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-heading">
						Edit User
						<a href="{{ url('/users') }}" class="btn btn-success btn-xs pull-right">
              <i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
						<button style="margin-right: 5px;" role="button" type="button" class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#deleteModal">
              <i class="fa fa-trash" aria-hidden="true"></i>
              Delete
            </button>
					</div>

					<div class="panel-body">
            <form action="{{ url('/users/'. $user->id) }}" method="POST">
                <input type="hidden" name="_method" value="patch">
                {{ csrf_field() }}
  							@if($errors->any())
  								<div class="alert alert-danger">
  									<ul>
  										{{ implode('', $errors->all('<li class="error">:message</li>')) }}
  									</ul>
  								</div>
  							@endif

											<!-- Col 1 -->
			                <div class="col-xs-6">
		                    <div class="form-group text-left <?php if($errors->has('name')) { echo 'has-error'; } ?>">
                          <label for="name">NAME</label>
		                      <input type="text" class="form-control input-text" id="name" name="name" value="{{ $user->name }}" />
		                    </div>
		                    <div class="form-group text-left <?php if($errors->has('email')) { echo 'has-error'; } ?>">
                          <label for="email">EMAIL</label>
                          <input type="text" class="form-control input-text" id="email" name="email" value="{{ $user->email }}" />
		                    </div>
					            </div><!-- End Col 1 -->

                      <div class="form-group text-right">
                          <button type="submit" class="btn btn-primary btn-sm">Save User</button>
                      </div>
			              </div><!-- End Col 2-->

						  </form>
					  </div>
					</div>
				</div> <!-- /.panel -->
			</div>
		</div>

	 </section> <!-- /.container -->


<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete User</h4>
      </div>
      <div class="modal-body">
      	<p>Are you sure you want to delete this user?</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">Cancel</button>
        <form role="form" method="POST" action="{{ url('/users/'. $user->id) }}" style="display: inline;">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
            <button style="margin-right: 5px;" type="submit" data-toggle="tooltip" data-placement="left" title="Delete" class="btn-success btn">
              <i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>

@stop
