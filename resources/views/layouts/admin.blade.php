<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ url('/') }}/css/admin-style.css" rel="stylesheet">
    <link href="{{ url('/') }}/css/font-awesome.min.css" rel="stylesheet">
    @yield('head')
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
  <section id="mag-wrap">
        <header id="header">
          <div class="navbar-default">
          <button type="button" class="navbar-toggle" id="nav_toggle">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        </div>
          <a href="#" class="navbar-brand logo">Ivie<span> Admin</span></a>

          <div class="top-nav">
            <ul class="nav pull-right top-menu">
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                        <div class="user_photo"><i class="fa fa-user-circle-o" aria-hidden="true"></i></div>
                        <span class="username">{{ Auth::user()->name }}</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout" role="menu">
                        <li>
                            <a href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                              <i class="fa fa-sign-out" aria-hidden="true"></i>  Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
                <!-- user login dropdown end -->
            </ul>
        </div>

        </header> <!-- /#header -->

          <aside>
            <div id="sidebar">
              <ul class="sidenav">
                <li class=""><a href="{{ url('/') }}/dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i>  Dashboard</a></li>
                <li class=""><a href="{{ url('/') }}/settings"><i class="fa fa-cogs" aria-hidden="true"></i></i>  Settings</a></li>
                <li class=""><a href="{{ url('/') }}/registrations"><i class="fa fa-users" aria-hidden="true"></i>  Registrations</a></li>
                <li class=""><a href="{{ url('/') }}/users"><i class="fa fa-users" aria-hidden="true"></i>  Users</a></li>
              </ul>

              </div> <!-- /#sidebar -->

          </aside> <!-- /aside -->

          <section id="page-content">

              @yield('content')

          </section> <!-- /#content -->

        <footer id="footer">

        </footer> <!-- /#footer -->

      </section> <!-- /#mag-wrap -->

    <!-- Scripts -->
    <script src="{{ url('/') }}/js/app.js"></script>
    @yield('script')
</body>
</html>
