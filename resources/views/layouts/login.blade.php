<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    {{-- <link href="{{ url('/') }}/css/app.css" rel="stylesheet"> --}}
    <link href="{{ url('/') }}/css/admin-style.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="login">

      <section id="mag-wrap">
        <header id="header">
          <div class="navbar-default">

        </div>
          <a href="#" class="navbar-brand logo">Ivie<span> Admin</span></a>

        </header> <!-- /#header -->



          <section id="page-content">

              @yield('content')

          </section> <!-- /#content -->

        <footer id="footer">

        </footer> <!-- /#footer -->

      </section> <!-- /#mag-wrap -->

    <!-- Scripts -->
    <script src="{{ url('/') }}/js/app.js"></script>
</body>
</html>
