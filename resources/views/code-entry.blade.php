@extends('layouts.app')

@section('title')
Code Entry
@endsection

@section('content')
<div class="container">
    <div class="row">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>

          @elseif( Session::get('error') )

              <div class="alert alert-danger">
                  <ul><li>{{ Session::get('error') }}</li></ul>
              </div>

           @elseif( Session::get('register_error') )
              <div class="alert alert-danger">
                  <ul><li>{{ Session::get('register_error') }}</li></ul>
              </div>
          @endif

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">FORM</div>

                <div class="panel-body">
                  <div class="form-group">
                    <form id="registration" name="registration" action="{{ url('/verify') }}" method="POST">
                     {{ csrf_field() }}
                     <label for="entry_code">REGISTER YOUR ENTRY CODE</label>
                     <input type="text" class="form-control input-text" id="entry_code" name="entry_code" placeholder="REGISTER YOUR ENTRY CODE HERE" />
                   </div>
                   <div class="form-group">
                      <input class="btn btn-primary" type="submit" value="SUBMIT">
                    </div>
                  </form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
