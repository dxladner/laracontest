@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Contest Closed</div>

                <div class="panel-body">

                  <h5 style="text-transform: inherit;font-size: 25px;font-weight: bold;">
                    Thanks for participating!<br>Check out <a href="www.albertsons.com">Albertsons.com</a> for great deals.
                  </h5>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
