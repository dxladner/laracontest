@extends('layouts.admin')

@section('title')
Dashboard
@stop

@section('head')

@stop

@section('content')
	<section class="wrapper">

		<div class="row">
      <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
              <div class="panel-heading">Registrations</div>

              <div class="panel-body btn_cta">
                <a href="{{ url('/') }}/registrations">
                  <span class="bg_blue">
                    <i class="fa fa-users fa-5x" aria-hidden="true"></i>
                  </span>
                  <span class="txt">
                    <h3>{{$num_registrations}} Registrations</h3>
                    Update/Edit Entrants
                  </span>
                </a>
              </div>
          </div>
      </div>

		</div> <!-- /.row -->

	 </section> <!-- /.container -->
@stop

@section('script')

@stop
