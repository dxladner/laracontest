@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>

          @elseif( Session::get('error') )

              <div class="alert alert-danger">
                  <ul><li>{{ Session::get('error') }}</li></ul>
              </div>

           @elseif( Session::get('register_error') )
              <div class="alert alert-danger">
                  <ul><li>{{ Session::get('register_error') }}</li></ul>
              </div>
          @endif

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">FORM</div>

                <div class="panel-body">
                  <form id="registration" name="registration" action="{{ url('/registration') }}" method="POST">
                     {{ csrf_field() }}
                    <div class="form-group">
                      <label for="first_name">First Name</label>
                      <input type="text" class="form-control" id="first_name" name="first_name" placeholder="FIRST NAME">
                      <label for="last_name">Last Name</label>
                      <input type="text" class="form-control" id="last_name" name="last_name" placeholder="LAST NAME">
                    </div>

                    <div class="form-group">
                      <label for="proclub_number">Proclub #</label>
                      <input type="text" class="form-control" id="proclub_number" name="proclub_number" placeholder="PROCLUB NUMBER">
                      <span style="font-size: 18px;color: red;">OR</span>
                      <label for="account_number">Account #</label>
                      <input type="text" class="form-control" id="account_number" name="account_number" placeholder="ACCOUNT NUMBER">
                    </div>

                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="EMAIL">
                    </div>

                    <div class="form-group">
                      <label for="address_one">Address One</label>
                      <input type="text" class="form-control" id="address_one" name="address_one" placeholder="ADDRESS ONE">
                    </div>

                    <div class="form-group">
                      <label for="address_two">Address Two</label>
                      <input type="text" class="form-control" id="address_two" name="address_two" placeholder="ADDRESS TWO">
                    </div>

                    <div class="form-group">
                      <label for="city">City</label>
                      <input type="text" class="form-control" id="city" name="city" placeholder="CITY">
                    </div>

                    <div class="form-group">
                      <label for="state">State/Province</label>
                      <select id="state" name="state" class="form-control">
             					  <option value="TX">Texas</option>
                        <option value="AL">Alabama</option>
                        <option value="LA">Louisiana</option>
                      </select>
                      <label for="zip">Zip/Postal Code</label>
                      <input type="text" class="form-control" id="zip" name="zip" placeholder="ZIP / POSTAL CODE">
                    </div>

                    <div class="form-group">
                      <label for="dob">DOB</label>
                      <input type="text" class="form-control" id="dob" name="dob" placeholder="DOB">
                    </div>

                    <div class="form-group">
                      <label for="profession">Profession</label>
                      <select id="profession" name="profession" class="form-control">
             					  <option value="Salon Owner">Salon Owner</option>
                        <option value="Cosmetologist">Cosmetologist</option>
                        <option value="Esthetician">Esthetician</option>
                        <option value="Nail Technician">Nail Technician</option>
                      </select>
                    </div>

                    <div class="form-group checkbox">
                      <label>
                        <input checked="checked" name="verify_receive_emails" type="checkbox" value="1" />
                        I would like to receive emails about special promotions, education courses, tips and trends.
                      </label>
                    </div>

                    <div class="form-group">
                      <input class="btn btn-primary btn-submit" type="submit" value="Submit">
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
