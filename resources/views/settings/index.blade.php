@extends('layouts.admin')

@section('title')
Contest Settings
@stop

@section('head')
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="{{ url('/') }}/css/datetimepicker.css">
<link rel="stylesheet" href="{{ url('/') }}/css/chosen.min.css">
@stop

@section('content')
	<section class="wrapper">

		<div class="row">
			<div class="col-sm-12">

				<div class="panel">
					<div class="panel-body">

						@if( Session::has('success') )
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							{{ Session::get('success') }}
						</div>
						@endif

						@if($errors->any())

                <div class="alert alert-danger">
                    <ul>
                         <li>
                           {{ implode('', $errors->all(':message')) }}
                         </li>
                    </ul>
                </div>

		            @elseif( Session::get('error') )

		                <div class="alert alert-danger">
		                    <ul><li>{{ Session::get('error') }}</li></ul>
		                </div>

		             @elseif( Session::get('register_error') )
		                <div class="alert alert-danger">
		                    <ul><li>{{ Session::get('register_error') }}</li></ul>
		                </div>
		            @endif

								@include('settings.partials.add-settings')
								<!-- EDIT CONTEST SETTINGS -->
								@include('settings.partials.edit-settings')

            <hr />

						@if($site_code_status->option_value == 'on')
	            <div class="panel panel-default">
	              <div class="panel-heading">
	                Promo Codes & Valid Dates
	                <a id="btn-add-code" href="" class="btn btn-success">Add Code</a>
	              </div>
	    					<div class="panel-body">

	    						<!-- ADD PROMO CODE -->
	    						<div class="container">
	    							<div class="row">
	    								@include('settings.partials.addcode')
	    							</div>
	    						</div>

	    						<!-- LOOP THROUGH PROMO CODES -->
	    						<table class="table table-striped table-hover">
	    							<thead>
	    							<tr>
	    								<th>Code Name</th>
	    								<th>Start Date</th>
	    								<th>End Date</th>
	    								<th>Options</th>
	    							</tr>
	    							</thead>
	    								@include('settings.partials.editcode')
	    						</table>
	    					</div>
						</div>
					@endif

				</div>
			</div> <!-- /.col-sm-12 -->
		</div> <!-- /.row -->
	 </section> <!-- /.container -->
@stop

@section('script')
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
  <script src="{{ url('/') }}/js/datetimepicker.js"></script>
  <script src="{{ url('/') }}/js/chosen.min.js"></script>
<script>

	//Add Code Form
	$('#btn-add-code').click(function() {
		$('#add-code').toggle();
		return false;
	});
	$('form#add-code #btn-cancel').click(function() {
		$('#add-code').toggle();
		$('#add-code').trigger("reset");
		return false;
	});

	//Edit Code Form
	$('.btn-edit-code').click(function() {
		var parent = $(this).parents('tr').get(0);
		$(parent).find('input').removeAttr('readonly');
		$(parent).find('select').prop("disabled", false);
		$(parent).find('select.chosen-select').prop("disabled", false).trigger("chosen:updated");
		$(parent).find('td.update-cancel').show();
		$(parent).find('.edit-delete').hide();

		$(parent).find(".datepicker").datetimepicker({
			timeFormat: 'HH:mm:ss',
			dateFormat: "yy-mm-dd",
			minDate: null
		});
		return false;
	});

	//Cancel Edit Code Form
	$('.btn-edit-cancel').click(function() {
		var parent = $(this).parents('tr').get(0);
		$(parent).find('input').attr('readonly', 'readonly');
		$(parent).find('select').prop("disabled", true);
		$(parent).find('select.chosen-select').prop("disabled", true).trigger("chosen:updated");
		$(parent).find('td.update-cancel').hide();
		$(parent).find('.edit-delete').show();
		$(parent).find(".datepicker").datetimepicker("destroy");
		return false;

	});

	//Datepicker for Add Code Form
	$('#add-code .datepicker').datetimepicker({
		timeFormat: 'HH:mm:ss',
		dateFormat: "yy-mm-dd",
		minDate: null
	});

</script>


<script type="text/javascript">
	var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"100%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  </script>
@stop
