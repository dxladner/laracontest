<div class="panel-heading">Turn On Code Functionality</div>
<table class="table table-striped table-hover">
<tr>
<form id="code-status" name="code-status" action="{{ url('/settings/codestatus') }}" method="POST">
  {{ csrf_field() }}

      <td>
          <select id="site_code_status" name="site_code_status" class="form-control">
            <option value="{{ $site_code_status->option_value }}">{{ $site_code_status->option_value }}</option>
            <option value="off">Off</option>
            <option value="on">On</option>
          </select>
      </td>
      <td class="update-cancel">
        <button type="submit" class="btn btn-info">Save</button>
        <a id="btn-cancel" class="btn btn-danger" href="">Cancel</a>
      </td>

</form>
</tr>
</table>
