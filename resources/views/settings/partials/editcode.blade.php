@if(!empty($codes[0]))
	@foreach($codes as $code)
		<tr>
      <form action="{{ url('/settings/'. $code->id) }}" method="POST">
          <input type="hidden" name="_method" value="patch">
          {{ csrf_field() }}
          <td><input class="form-control" readonly type="text" name="code_name" value="{{ $code->code_name }}" /></td>
          <td><input class="form-control datepicker" readonly type="text" name="start_date" value="{{ $code->start_date }}"  /></td>
          <td><input class="form-control datepicker" readonly type="text" name="end_date" value="{{ $code->end_date }}"  /></td>

          <td class="update-cancel" style="display: none;">
            <button type="submit" class="btn btn-success btn-submit-code">Update</button>
  					<a class="btn btn-danger btn-edit-cancel" href="">Cancel</a>
  				</td>
        </form>

  				<td class="edit-delete">
  					<a href="" data-toggle="tooltip" data-placement="left" title="Edit" class="btn-edit-code btn btn-info">Edit</a>
            <form role="form" method="POST" action="{{ url('/settings/'. $code->id) }}" style="display: inline;">
                <input type="hidden" name="_method" value="delete">
                {{ csrf_field() }}
  				      <button type="submit" data-toggle="tooltip" data-placement="left" title="Delete" class="btn-danger btn btn-info">Delete</button>
  				  </form>
  				</td>
		</tr>
	@endforeach
@else
	<tr>
		<td colspan="5">
			<div class="alert alert-info">No codes were found.</div>
		</td>
	</tr>

@endif
