
<form id="add-code" name="add-code" action="{{ url('/settings') }}" method="POST" style="display: none;">
  {{ csrf_field() }}
	<div class="row">
		<!-- Col 1 -->
        <div class="col-xs-3">
            <div class="form-group text-left <?php if($errors->has('code_name')) { echo 'has-error'; } ?>">
              <label>CODE NAME</label>
              <input id="code_name" name="code_name" type="text" class="form-control input-text" />
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group text-left <?php if($errors->has('code_name')) { echo 'has-error'; } ?>">
              <label>START DATE</label>
              <input id="start_date" name="start_date" type="text" class="form-control input-text datepicker" />
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group text-left <?php if($errors->has('code_name')) { echo 'has-error'; } ?>">
              <label>END DATE</label>
              <input id="end_date" name="end_date" type="text" class="form-control input-text datepicker" />
            </div>
            <button type="submit" class="btn btn-info">Add Code</button>
            <a id="btn-cancel" class="btn btn-danger" href="">Cancel</a>
        </div>
	</div><!--End Row-->

</form>
