<div class="panel-heading">Contest Settings</div>
<table class="table table-striped table-hover">
	<thead>
	<tr>
		<th>Contest Start Date</th>
		<th>Contest End Date</th>
		<th>Time Zone</th>
		<th>Options</th>
	</tr>
	</thead>
	<tr>
	   <form id="edit-settings" name="edit-settings" action="{{ url('/settings/main') }}" method="POST">
			   {{ csrf_field() }}
       <td><input class="datepicker form-control" readonly type="text" id="contest_start_date" name="contest_start_date" value="{{ $contest_start_date->option_value }}"  /></td>
       <td><input class="datepicker form-control" readonly type="text" id="contest_end_date" name="contest_end_date" value="{{ $contest_end_date->option_value }}"  /></td>
       <td>
         <select id="time_zone" name="time_zone" class="form-control" disabled>
					 <option value="{{ $time_zone->option_value }}">{{ $time_zone->option_value }}</option>
           <option value="PST">PST</option>
           <option value="MST">MST</option>
           <option value="CST">CST</option>
           <option value="EST">EST</option>
         </select>
       </td>
      <td class="update-cancel" style="display: none;">
        <button class="btn btn-success btn-submit-code">Update</button>
        <a class="btn btn-danger btn-edit-cancel" href="">Cancel</a>
      </td>
     </form>

     <td class="edit-delete">
 			<a href="" data-toggle="tooltip" data-placement="left" title="Edit" class="btn-edit-code btn btn-info">Edit</a>
 		</td>

	</tr>

</table>
