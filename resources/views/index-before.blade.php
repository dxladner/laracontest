@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Contest Not Open</div>

                <div class="panel-body">

                  <h5 style="text-transform: inherit;font-size: 25px;font-weight: bold;">
                    Contest has not started yet. Please check back. Contest starts on DATE
                  </h5>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
