<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;

class UserController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
      $users = User::all();
      return view('users.index', compact('users'));
    }

    public function create()
    {
        return view('users.create');
    }

    public function edit($id)
    {
      $user = User::findOrFail($id);
      return view('users.edit', compact('user'));
    }

    public function store()
  	{

  		$input  = Input::all();

  		$rules = array(
        'name' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|min:6|confirmed',
  			);

  		$v = Validator::make($input, $rules);

  		if($v->passes())
  		{

  			$user = new User();
  			$user->name = $input['name'];
  			$user->email = $input['email'];
  			$user->password = bcrypt($input['password']);

  			$user->save();

  			return Redirect::to('users')->with('success', 'User has been created successfully.');

  		}
      else
      {
  			return Redirect::to('users')->withInput()->withErrors($v);
  		}

  	}

    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->save();

    		return Redirect::to('users')->with('success', 'User has been updated successfully.');

    }

    public function destroy($id)
    {
      $user = User::findOrFail($id);
      $user->delete();
      $users = User::all();
      return view('users.index', compact('users'));
    }
}
