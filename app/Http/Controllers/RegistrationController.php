<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registration;
use Illuminate\Support\Facades\Input;
use Redirect;
use Carbon\Carbon;
use Validator;
use DB;
use Session;
use Excel;
use App\Code;

class RegistrationController extends Controller
{
    public function index()
    {
        $input  = Input::all();

		    $download_url = '';

  		  //Filter by Registration date
  		  if( Input::has('date_start') && Input::has('date_end') )
        {
			       $from = date( 'Y-m-d H:i:s', strtotime( $input['date_start'] . ' ' . '00:00:00' ));
             $to = date( 'Y-m-d H:i:s', strtotime( $input['date_end'] . ' ' . '23:59:59'  ));

			       Session::flash( 'date_start', $input['date_start']);
			       Session::flash( 'date_end', $input['date_end']);

			       $registrations = Registration::whereBetween('created_at', array($from, $to))->paginate(20);
		     }
         else
         {
			        $registrations = Registration::orderBy('id', 'DESC')->paginate(20);
		     }

         return view('registrations.index', compact('registrations'));
    }

    public function create()
    {
        return view('registrations.create');
    }

    public function store()
    {
      $input = Input::only('first_name', 'last_name', 'proclub_number', 'account_number', 'email', 'address_one', 'address_two', 'city', 'state', 'zip', 'dob', 'profession', 'verify_receive_emails');

      $rules = array(
        'first_name'	        =>	'required',
        'last_name'	          =>	'required',
        'proclub_number'	    =>	'nullable|digits:12',
        'account_number'      =>  'nullable|digits:8',
        'email'	              =>	'required|email',
        'address_one'	        =>	'required',
        'address_two'	        =>	'nullable',
        'city'	              =>	'required',
        'state'	              =>	'required',
        'zip'	                =>  'required',
        'dob'	                =>	'required',
        'profession'	        =>	'required',
        'verify_receive_emails' =>  'nullable',
      );

      $v = Validator::make($input, $rules);

      if($v->passes())
      {
        if(empty($input['proclub_number']) && empty($input['account_number']))
        {
          return Redirect::to( '/registrations/create' )->withInput()->withErrors("Either the Proclub or Account Number must be filled out in order to submit the form");
        }
        if(empty($input['proclub_number']))
        {
          $subscriber = DB::table('registrations')
                        ->Where('account_number', '=', $input['account_number'])
                        ->get();
        }
        else
        {
          $subscriber = DB::table('registrations')
                        ->Where('proclub_number', '=', $input['proclub_number'])
                        ->get();
        }

        if( $subscriber->count() )
        {
          return Redirect::to( '/registrations/create' )->withInput()->withErrors("You already have been register for this week for a chance to win contest");
        }
        else
        {
          $subscriber = new Registration();
          $subscriber->first_name = $input['first_name'];
          $subscriber->last_name = $input['last_name'];
          $subscriber->proclub_number = $input['proclub_number'];
          $subscriber->account_number = $input['account_number'];
          $subscriber->email = $input['email'];
          $subscriber->address_one = $input['address_one'];
          $subscriber->address_two = $input['address_two'];
          $subscriber->city = $input['city'];
          $subscriber->state = $input['state'];
          $subscriber->zip = $input['zip'];
          $subscriber->dob = $input['dob'];
          $subscriber->profession = $input['profession'];

          if(Input::has('verify_receive_emails'))
          {
            $subscriber->verify_receive_emails = $input['verify_receive_emails'];
          }

          $subscriber->save();

          return Redirect::to('registrations')->with('success', 'Thank You' . $input['first_name'] . ' ' . $input['last_name']);
        }

      }
      else
      {
          return Redirect::to('/registrations/create')->withInput()->withErrors($v);
      }
    }

    public function show($id)
    {
      $registration = Registration::findOrFail($id);
      return view('registrations.edit', compact('registration'));
    }

    public function edit($id)
    {
      $registration = Registration::findOrFail($id);
      return view('registrations.edit', compact('registration'));
    }

    public function update($id)
    {
      $input = Input::only('first_name', 'last_name', 'proclub_number', 'account_number', 'email', 'verify_receive_emails', 'address_one', 'address_two', 'city', 'state', 'zip', 'dob', 'profession');

      $rules = array(
        'first_name'	        =>	'required',
        'last_name'	          =>	'required',
        'proclub_number'	    =>	'nullable|digits:12',
        'account_number'      =>  'nullable|digits:8',
        'email'	              =>	'required|email',
        'address_one'	        =>	'required',
        'address_two'	        =>	'nullable',
        'city'	              =>	'required',
        'state'	              =>	'required',
        'zip'	                =>  'required',
        'dob'	                =>	'required',
        'profession'	        =>	'required',
        'verify_receive_emails' =>  'nullable',
      );

      $v = Validator::make($input, $rules);

      if($v->passes())
      {
        if(empty($input['proclub_number']) && empty($input['account_number']))
        {

					return Redirect::to( 'registrations.index' )->withInput()->withErrors("Either the Proclub or Account Number must be filled out in order to submit the form");
				}
        if(empty($input['proclub_number']))
        {
          $subscriber = DB::table('registrations')
                        ->Where('account_number', '=', $input['account_number'])
                        ->get();
        }
        else
        {
          $subscriber = DB::table('registrations')
                        ->Where('proclub_number', '=', $input['proclub_number'])
                        ->get();
        }

        if( $subscriber->count() )
        {
          return Redirect::to( 'registrations.index' )->withInput()->withErrors("This email has already been register for this week for a chance to win contest");
        }
        else
        {
          $subscriber = Registration::find($id);
          $subscriber->first_name = $input['first_name'];
          $subscriber->last_name = $input['last_name'];
          $subscriber->proclub_number = $input['proclub_number'];
          $subscriber->account_number = $input['account_number'];
          $subscriber->email = $input['email'];
          $subscriber->address_one = $input['address_one'];
          $subscriber->address_two = $input['address_two'];
          $subscriber->city = $input['city'];
          $subscriber->state = $input['state'];
          $subscriber->zip = $input['zip'];
          $subscriber->dob = $input['dob'];
          $subscriber->profession = $input['profession'];

          if(Input::has('verify_receive_emails'))
          {
            $subscriber->verify_receive_emails = $input['verify_receive_emails'];
          }

          $subscriber->save();

          return Redirect::to('registrations.index')->with('success', 'You successfully edited' . $input['first_name'] . ' ' . $input['last_name']);
        }

      }
      else
      {
          return Redirect::to('registrations.index')->withInput()->withErrors($v);
      }
    }

    public function destroy($id)
    {
      $registration = Registration::findOrFail($id);
      $registration->delete();
      $num_registrations = DB::table('registrations')->count();

      return view('dashboard', compact('num_registrations'));
    }

    public function search(Request $request)
    {
      $input  = Input::all();

      $query = $input['q'];

      $registrations = array();
      if($query)
      {
        $registrations = Registration::where('first_name', 'LIKE', "%$query%")
        ->orWhere('last_name', 'LIKE', "%$query")
        ->paginate(20);
      }

      return view('registrations.search', compact('registrations'));
    }


    /**
     * Register.
     *
     * @return Response
     */
    public function register()
    {
      $input = Input::only('first_name', 'last_name', 'proclub_number', 'account_number', 'email', 'address_one', 'address_two', 'city', 'state', 'zip', 'dob', 'profession', 'verify_receive_emails');

      $rules = array(
        'first_name'	        =>	'required',
        'last_name'	          =>	'required',
        'proclub_number'	    =>	'nullable|digits:12',
        'account_number'      =>  'nullable|digits:8',
        'email'	              =>	'required|email',
        'address_one'	        =>	'required',
        'address_two'	        =>	'nullable',
        'city'	              =>	'required',
        'state'	              =>	'required',
        'zip'	                =>  'required',
        'dob'	                =>	'required',
        'profession'	        =>	'required',
        'verify_receive_emails' =>  'nullable',
      );

      // $messages = [
		  //   'member_numbers.required' => 'You must enter either your Proclub or account member number in order to submit the form.'
		  // ];

      $v = Validator::make($input, $rules);

      if($v->passes())
      {
        if(empty($input['proclub_number']) && empty($input['account_number']))
        {

					return Redirect::to( '/' )->withInput()->withErrors("Either the Proclub or Account Number must be filled out in order to submit the form");
				}
        if(empty($input['proclub_number']))
        {
          $subscriber = DB::table('registrations')
                        ->Where('account_number', '=', $input['account_number'])
                        ->get();
        }
        else
        {
          $subscriber = DB::table('registrations')
                        ->Where('proclub_number', '=', $input['proclub_number'])
                        ->get();
        }

        if( $subscriber->count() )
        {
          return Redirect::to( '/' )->withInput()->withErrors("You already have been register for this week for a chance to win contest");
        }
        else
        {
          $subscriber = new Registration();
          $subscriber->first_name = $input['first_name'];
          $subscriber->last_name = $input['last_name'];
          $subscriber->proclub_number = $input['proclub_number'];
          $subscriber->account_number = $input['account_number'];
          $subscriber->email = $input['email'];
          $subscriber->address_one = $input['address_one'];
          $subscriber->address_two = $input['address_two'];
          $subscriber->city = $input['city'];
          $subscriber->state = $input['state'];
          $subscriber->zip = $input['zip'];
          $subscriber->dob = $input['dob'];
          $subscriber->profession = $input['profession'];

          if(Input::has('verify_receive_emails'))
          {
            $subscriber->verify_receive_emails = $input['verify_receive_emails'];
          }

          $subscriber->save();

          return Redirect::to('thankyou')->with('success', 'Thank You' . $input['first_name'] . ' ' . $input['last_name']);
        }

      }
      else
      {
          return Redirect::to('/')->withInput()->withErrors($v);
      }
    }


    public function reports()
  	{

  		$input  = Input::only('date_start', 'date_end');

  		$rules = array(
  			'date_start'	=>	'required',
  			'date_end'	=>	'required',
  			);

  		$v = Validator::make($input, $rules);

  		if($v->passes())
  		{
  			$from = date( 'Y-m-d H:i:s', strtotime( $input['date_start'] . ' ' . '00:00:00' ));
  			$to = date( 'Y-m-d H:i:s', strtotime( $input['date_end'] . ' ' . '23:59:59'  ));

  			Session::flash( 'date_start', $input['date_start']);
  			Session::flash( 'date_end', $input['date_end']);

  			$fileName = "CosmoProf-OOhLaLA";

  			$registrations = Registration::whereBetween('created_at', array($from, $to))->get();

  			Excel::create($fileName, function($excel) use($registrations)
        {

  			     $title = "Registration Report";

  			    // Set the title
  			    $excel->setTitle( $title );

  			    // Chain the setters
  			    $excel->setCreator('Ivie')
  			          ->setCompany('Ivie Inc');

  			    // Call them separately
  			    $excel->setDescription("Registration Report");


  			    $excel->sheet('Sheetname', function($sheet) use($registrations)
            {

  			    	$sheet->cells('A1:N1', function($cells)
              {
  			    		// Set background color
  			    		$cells->setBackground('#DDDDDD');
  					    // Set with font color
  						  $cells->setFontColor('#000000');

  					  });

  			    	$data[] = [
  			    		'First Name' => '',
  			    		'Last Name' => '',
                'Proclub Number' => '',
                'Account Number' => '',
  			    		'Email' => '',
  			    		'Address One' => '',
                'Address Two' => '',
  			    		'City' => '',
  			    		'State' => '',
  			    		'Zip' => '',
  			    		'DOB' => '',
  			    		'Profession' => '',
                'Verify Receive E-Mails' => '',
  			    		'Created At' => ''
  		    		];

  					foreach ($registrations as $registration) {

  						$data[] =
  						[
  							$registration->first_name,
  							$registration->last_name,
  							$registration->proclub_number,
                $registration->account_number,
  							$registration->email,
  							$registration->address_one,
                $registration->address_two,
  							$registration->city,
  							$registration->state,
  							$registration->zip,
  							$registration->dob,
  							$registration->profession,
  							$registration->verify_receive_emails,
  							$registration->created_at->format('m/j/Y')
  						];
  					}

  			    $sheet->fromArray($data);

  			    });


  			})->export('xlsx');

  		}
      else
      {
  			   return Redirect::to('registrations')->withInput()->withErrors($v);
  		}
  	}

    /**
  	 * Verify code
  	 *
  	 * @return Response
  	 */
  	public function verify()
  	{

  		$input = Input::only('entry_code');

  		$rules = array(
  			'entry_code'	=>	'required',
  			);

  		$v = Validator::make($input, $rules);

  		if($v->passes())
  		{

  			$allow_registration = false;
  			$error_message = "";

  			//Check if code exists
  			$user_code = strtolower($input['entry_code']);
  			$code = Code::where('code_name', $user_code)->first(); //might be a problem if they select same code for different dates
  			if($code)
  			{
  				$timezone = DB::table('settings')->where('option_name', 'time_zone')->first();
  				$now = Carbon::now($timezone->option_value);
  				//$now = Carbon::create(2015, 02, 13, 0); //For dev testing

  				//Check if code is expired
  				$code_start_date = Carbon::createFromTimestamp(strtotime($code->start_date));
  				$code_end_date = Carbon::createFromTimestamp(strtotime($code->end_date));

  				if( Carbon::create($now->year, $now->month, $now->day, 0)->between( $code_start_date, $code_end_date ) )
  				{
  					$allow_registration = true;
  				}
          else
          {
  					$error_message = "Invalid Code";
  				}
  			}
        else
        {
  				$error_message = "Invalid Code. ";
  			}


  			if( $allow_registration )  // Allow Registation
  			{

  				return Redirect::to('codeform')->with(array('entry_code' => $user_code));
  			}
        else
  			{
  				return Redirect::to('code')->withInput()->with('error', $error_message);
  			}
  		}
      else
      {

  			return Redirect::to('code')->withInput()->withErrors($v);
  		}

  	}
    public function codeform()
    {
      $input = Input::only('entry_code');
      $entry_code = strtolower($input['entry_code']);

      return view('code-form', compact('entry_code'));
    }
    /**
  	 * Registration with entry code.
  	 *
  	 * @return Response
  	 */
    	public function registration()
    	{
    		$input = Input::only('entry_code', 'first_name', 'last_name', 'email', 'address', 'city', 'state', 'zip', 'phone', 'earn_code', 'verify_age', 'verify_terms_of_use', 'verify_receive_emails', 'store');

    		$rules = array(
    			'first_name'	=>	'required',
    			'last_name'	=>	'required',
    			'email'	=>	'required|email',
    			'address'	=>	'required',
    			'city'	=>	'required',
    			'state'	=>	'required',
    			'zip'	=> 'required',
    			'phone'	=>	'required|min:10|max:12',
    			'earn_code'	=>	'required',
    			'store'	=>	'required',
    			'verify_age' => 'required',
    			'verify_terms_of_use' => 'required',
    		);

    		$v = Validator::make($input, $rules);

    		$valid_states = get_states();

    		if($v->passes())
    		{

    			$word = strtolower($input['entry_code']);

    			$subscriber = Registration::where('email', '=', $input['email'])->where('entry_code', '=', $word)->get();

    			if( $subscriber->count() ) {
    				$valid_states = get_states();
    				return Redirect::to('registration')->withInput()->with('registration_code', 'dallascowboysregistration')->with(array('valid_states' => $valid_states, 'register_error' => 'You already have been register for this week for a chance to win Dallas Cowboys home game tickets.'));
    			} else {
    				$subscriber = new Registration();
    				$subscriber->first_name = $input['first_name'];
    				$subscriber->last_name = $input['last_name'];
    				$subscriber->email = $input['email'];
    				$subscriber->entry_code = $word;
    				$subscriber->address = $input['address'];
    				$subscriber->city = $input['city'];
    				$subscriber->state = $input['state'];
    				$subscriber->zip = $input['zip'];
    				$subscriber->phone = str_replace(array("-", "*", "_", " "),"",$input['phone']);
    				$subscriber->store = $input['store'];
    				$subscriber->earn_code = $input['earn_code'];

    				$subscriber->verify_age = $input['verify_age'];
    				$subscriber->verify_terms_of_use = $input['verify_terms_of_use'];

    				if(Input::has('verify_receive_emails')) {
    					$subscriber->verify_receive_emails = $input['verify_receive_emails'];
    				}

    				$subscriber->ip_address = Request::server('REMOTE_ADDR');
    				$subscriber->save();

    				return Redirect::to('thankyou')->with('success', 'Thank You' . $input['first_name'] . ' ' . $input['last_name']);
    			}

    		} else {
    				return Redirect::to('registration')->withInput()->withErrors($v)->with('registration_code', 'dallascowboysregistration')->with(array('entry_code', 'dallascowboysregistration', 'valid_states' => $valid_states));
    		}
    	}

}

//Get list of valid states
function get_states(){
	$valid_states = DB::table('settings')->where('option_name', 'valid_states')->first(); 		$states_list = array();
	$all_states = (Config::get('constants.USA_STATES'));
	$valid_states = unserialize($valid_states->option_value);
	foreach($valid_states as $state) {
		$states_list[$state] = $all_states[$state];
	}
	return $states_list;
}
