<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
      $num_registrations = DB::table('registrations')->count();
      return view('dashboard', compact('num_registrations'));
    }
}
