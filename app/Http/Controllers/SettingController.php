<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use App\Code;
use Carbon\Carbon;

class SettingController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
        $codes = Code::orderBy('start_date', 'ASC')->get();
        $settings = DB::table('settings')->get();
		    $contest_start_date = DB::table('settings')->where('option_name', 'contest_start_date')->first();
		    $contest_end_date = DB::table('settings')->where('option_name', 'contest_end_date')->first();
		    $time_zone = DB::table('settings')->where('option_name', 'time_zone')->first();
        $site_code_status = DB::table('settings')->where('option_name', 'site_code_status')->first();

        return view('settings.index', compact('codes', 'contest_start_date', 'contest_end_date', 'time_zone', 'site_code_status'));
    }

    public function codestatus(Request $request)
    {
        $input  = Input::all();

        $rules = array(
          'site_code_status'	=>	'required',
          );

        $session_code_status =  $input['site_code_status'];

        $v = Validator::make($input, $rules);
        if($v->passes())
    		{
          //DB::insert('insert into settings (option_name, option_value) values (?, ?)', ['site_code_status', $input['site_code_status']]);
          DB::table('settings')->where('option_name', 'site_code_status')->update(array('option_value' => $input['site_code_status']));
          $request->session()->forget('site_code_status');
          session(['site_code_status' => $session_code_status]);
        
          return Redirect::to('settings')->with('success', 'Settings have been updated successfully.');
        }
        else
        {
          return Redirect::to('settings')->withInput()->withErrors($v);
        }

    }
    // // uncomment when starting a website
    // public function create(Request $request)
  	// {
    //
    //   $input  = Input::all();
    //
  	// 	$rules = array(
  	// 		'contest_start_date'	=>	'required',
  	// 		'contest_end_date'	=>	'required',
  	// 		'time_zone'	=>	'required',
  	// 		);
    //
  	// 	$v = Validator::make($input, $rules);
    //
  	// 	if($v->passes())
  	// 	{
    //     DB::insert('insert into settings (option_name, option_value) values (?, ?)', ['contest_start_date', $input['contest_start_date']]);
    //     DB::insert('insert into settings (option_name, option_value) values (?, ?)', ['contest_end_date', $input['contest_end_date']]);
    //     DB::insert('insert into settings (option_name, option_value) values (?, ?)', ['time_zone', $input['time_zone']]);
    //
  	// 		return Redirect::to('settings')->with('success', 'Settings have been updated successfully.');
    //
  	// 	} else {
    //
  	// 		return Redirect::to('settings')->withInput()->withErrors($v);
    //
  	// 	}
  	// }

  	public function main(Request $request)
  	{

      $input  = Input::all();

  		$rules = array(
  			'contest_start_date'	=>	'required',
  			'contest_end_date'	=>	'required',
  			'time_zone'	=>	'required',
  			);

  		$v = Validator::make($input, $rules);

  		if($v->passes())
  		{

  			     DB::table('settings')->where('option_name', 'contest_start_date')
              ->update(array('option_value' => $input['contest_start_date']));

              DB::table('settings')->where('option_name', 'contest_end_date')
              ->update(array('option_value' => $input['contest_end_date']));

              DB::table('settings')->where('option_name', 'time_zone')
              ->update(array('option_value' => $input['time_zone']) );

  			return Redirect::to('settings')->with('success', 'Settings have been updated successfully.');

  		} else {

  			return Redirect::to('settings')->withInput()->withErrors($v);

  		}
  	}

    public function store()
  	{

  		$input  = Input::all();

  		$rules = array(
  			'code_name'			=>	'required',
        'start_date'			=>	'required',
        'end_date'			=>	'required',
  			);

  		$v = Validator::make($input, $rules);

  		if($v->passes())
  		{

  			$code = new Code();
  			$code->code_name = $input['code_name'];
  			$code->start_date = $input['start_date'];
  			$code->end_date = $input['end_date'];

  			$code->save();


  			return Redirect::to('settings')->with('success', 'Code has been created successfully.');

  		} else {

  			return Redirect::to('settings')->withInput()->withErrors($v);

  		}

  	}

  	public function update($id)
  	{

  		$input  = Input::all();

  		$rules = array(
  			'code_name'	=>	'required',
  			'start_date'	=>	'required',
  			'end_date'	=>	'required',
  			);

  		$v = Validator::make($input, $rules);

  		if($v->passes())
  		{
  			$code = Code::find($id);
  			$code->code_name = $input['code_name'];
  			$code->start_date = $input['start_date'];
  			$code->end_date = $input['end_date'];
  			$code->save();

  			return Redirect::to('settings')->with('success', 'Code has been updated successfully.');

  		} else {

  			return Redirect::to('settings')->withInput()->withErrors($v);

  		}
  	}

    public function destroy($id)
  	{
  	   Code::destroy($id);

   	   return Redirect::to('settings')->withInput()->with('success', 'Code has been deleted successfully.');
  	}


}
