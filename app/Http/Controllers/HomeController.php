<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Session;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $start = DB::table('settings')->where('option_name', 'contest_start_date')->first();
      $end = DB::table('settings')->where('option_name', 'contest_end_date')->first();
      $timezone = DB::table('settings')->where('option_name', 'time_zone')->first();
      $session_code_status_db = DB::table('settings')->where('option_name', 'site_code_status')->first();
      $session_code_status = $session_code_status_db->option_value;
      $todays_date = Carbon::now($timezone->option_value);
      $contest_start_date = Carbon::createFromTimestamp(strtotime($start->option_value));
      $contest_end_date = Carbon::createFromTimestamp(strtotime($end->option_value));

      if($todays_date->lt($contest_start_date))
      {
        return view('index-before');
      }
      else if($todays_date->gt($contest_end_date))
      {
        return view('index-after');
      }
      else
      {
        if($session_code_status == 'on')
        {
          return view('code-entry');
        }
        else
        {
          return view('index');
        }
      }
    }
}
