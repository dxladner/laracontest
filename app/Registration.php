<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $fillable = [];

    protected $table = 'registrations';

    public function fullName()
    {
      return $this->first_name . ' ' . $this->last_name;
    }

    // public function store()
    // {
    //     return $this->hasOne('Store');
    // }
}
