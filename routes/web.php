<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Site Routes
Route::get('/', 'HomeController@index');
Route::post('registration', 'RegistrationController@register');
Route::get('thankyou', function()
{
	return View::make('thankyou');
});

Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');

Route::get('register', 'Auth\RegisterController@showRegistrationForm');
Route::post('register', 'Auth\RegisterController@register');

// contest sites with codes
Route::get('code', function()
{
	return View::make('code-entry');
});
Route::post('verify', 'RegistrationController@verify');
Route::get('codeform', 'RegistrationController@codeform');
Route::post('codeform', 'RegistrationController@registration');


Route::get('/registrations/reports', 'RegistrationController@reports');

// Admin routes
Route::group(['middleware' => 'auth'], function () {
	Route::get('/registrations', 'RegistrationController@index');
	Route::get('/registrations/create', 'RegistrationController@create');
	Route::post('/registrations/create', 'RegistrationController@store');

	Route::get('/registrations/{id}', 'RegistrationController@show');
	Route::get('/registrations/{id}/edit', 'RegistrationController@edit');
	Route::post('/registrations/edit/{id}', 'RegistrationController@update');
	Route::delete('/registrations/delete/{id}', 'RegistrationController@destroy');
	Route::post('/registrations/search', 'RegistrationController@search');

});

// Admin routes Auth controlled by controller
Route::get('/home', 'HomeController@index');
Route::get('/dashboard', 'DashboardController@index');
Route::get('/settings', 'SettingController@index');
Route::post('settings/main', 'SettingController@main');
Route::post('/settings/codestatus', 'SettingController@codestatus');
Route::resource('settings', 'SettingController');
Route::resource('users', 'UserController');
Route::get('/winners', 'WinnerController@index');

// uncomment when starting a website as you need to set an initial contest date setting
//Route::post('settings/create', 'SettingController@create');
