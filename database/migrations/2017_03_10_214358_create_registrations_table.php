<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
          $table->increments('id');
          $table->string('first_name');
          $table->string('last_name');
          $table->string('proclub_number');
          $table->string('account_number');
          $table->string('email')->unique();
          $table->boolean('verify_receive_emails')->default(false);
          $table->string('address_one');
          $table->string('address_two');
          $table->string('city');
          $table->string('state');
          $table->string('zip');
          $table->string('dob');
          $table->string('profession');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
