<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
           'option_name' => 'contest_start_date',
           'option_value' => '2017-03-16 00:00:00',
        ]);
        DB::table('settings')->insert([
            'option_name' => 'contest_end_date',
            'option_value' => '2017-03-31 00:00:00',
        ]);
        DB::table('settings')->insert([
            'option_name' => 'time_zone',
            'option_value' => 'CST',
        ]);
        DB::table('settings')->insert([
             'option_name' => 'site_code_status',
             'option_value' => 'off',
        ]);
    }
}
